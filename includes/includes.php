<?php
include 'blogpost.php';

$ini = parse_ini_file(__DIR__ . '/config.ini', true);

// Change this info so that it works with your system.
// $connection = mysqli_connect($ini['db']['server'], $ini['db']['username'], $ini['db']['password']) or die ("<p class='error'>Sorry, we were unable to connect to the database server.</p>");
// $database = $ini['db']['database'];
// mysqli_select_db($connection, $database) or die ("<p class='error'>Sorry, we were unable to connect to the database.</p>");

$link = mysqli_connect(
	$ini['db']['server'],
	$ini['db']['username'],
	$ini['db']['password'],
	$ini['db']['database']
);

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

function GetBlogPosts($link, $inId=null, $inTagId =null)
{
	if (!empty($inId))
	{
		$query = mysqli_query($link, "SELECT * FROM blog_posts WHERE id = " . $inId . " ORDER BY id DESC"); 
	}
	else if (!empty($inTagId))
	{
		$query = mysqli_query($link, "SELECT blog_posts.* FROM blog_post_tags LEFT JOIN (blog_posts) ON (blog_post_tags.blog_post_id = blog_posts.id) WHERE blog_post_tags.tag_id =" . $inTagId . " ORDER BY blog_posts.id DESC");
	}
	else
	{
		$query = mysqli_query($link, "SELECT * FROM blog_posts ORDER BY id DESC");
	}
	
	$postArray = array();
	while ($row = mysqli_fetch_assoc($query))
	{
		$myPost = new BlogPost($link, $row['id'], $row['title'], $row['post'], $row['post'], $row["author_id"], $row["date_posted"]);
		$postArray[]= $myPost;
	}
	return $postArray;
}
?>